
#include "config.h"

#include <sys/types.h>
#include <sys/stat.h> /* for struct stat */

#ifdef HAVE_STRING_H
#include <string.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif

#include <errno.h>

#include <ne_request.h>
#include <ne_alloc.h>
#include <ne_utils.h>
#include <ne_basic.h>
#include <ne_207.h>
#include <ne_props.h>
#include "necd_basic.h"

#ifdef NE_HAVE_DAV
#include <ne_uri.h>
#include <ne_locks.h>
#endif

#include <ne_dates.h>


static const struct options_map {
    const char *name;
    unsigned int cap;
} options_map[] = {
    { "1",                                    NE_CAP_DAV_CLASS1 },
    { "2",                                    NE_CAP_DAV_CLASS2 },
    { "3",                                    NE_CAP_DAV_CLASS3 },
    { "<http://apache.org/dav/propset/fs/1>", NE_CAP_MODDAV_EXEC },
    { "access-control",                       NE_CAP_DAV_ACL },
    { "version-control",                      NE_CAP_VER_CONTROL },
    { "checkout-in-place",                    NE_CAP_CO_IN_PLACE },
    { "version-history",                      NE_CAP_VER_HISTORY },
    { "workspace",                            NE_CAP_WORKSPACE },
    { "update",                               NE_CAP_UPDATE },
    { "label",                                NE_CAP_LABEL },
    { "working-resource",                     NE_CAP_WORK_RESOURCE },
    { "merge",                                NE_CAP_MERGE },
    { "baseline",                             NE_CAP_BASELINE },
    { "version-controlled-collection",        NE_CAP_VC_COLLECTION },
    { "calendar-access",                      NECD_CAP_CALENDAR_ACCESS },
    { "calendar-schedule",                    NECD_CAP_CALENDAR_SCHEDULE },
    { "calendar-auto-schedule",               NECD_CAP_CALENDAR_SCHEDULE_AUTO },
    { "calendar-proxy",                       NECD_CAP_CALENDAR_PROXY },
    { "extended-mkcol",                       NECD_CAP_EXTENDED_MKCOL },
    { "bind",                                 NECD_CAP_BIND },
    { "addressbook",                          NECD_CAP_ADDRESS_BOOK }
};

static void parse_dav_header(const char *value, unsigned int *caps)
{
    char *tokens = ne_strdup(value), *pnt = tokens;
    
    *caps = 0;

    do {
        char *tok = ne_qtoken(&pnt, ',',  "\"'");
        unsigned n;

        if (!tok) break;
        
        tok = ne_shave(tok, " \r\t\n");

        for (n = 0; n < sizeof(options_map)/sizeof(options_map[0]); n++) {
            if (strcmp(tok, options_map[n].name) == 0) {
                *caps |= options_map[n].cap;
            }
        }
    } while (pnt != NULL);
    
    ne_free(tokens);
}

int necd_options2(ne_session *sess, const char *uri, unsigned int *caps)
{
    ne_request *req = ne_request_create(sess, "OPTIONS", uri);
    int ret = ne_request_dispatch(req);
    const char *header = ne_get_response_header(req, "DAV");
    
    if (header) parse_dav_header(header, caps);
 
    if (ret == NE_OK && ne_get_status(req)->klass != 2) {
	ret = NE_ERROR;
    }
    
    ne_request_destroy(req);

    return ret;
}

int necd_options(ne_session *sess, const char *path,
               necd_server_capabilities *caps)
{
    int ret;
    unsigned int capmask = 0;
    
    memset(caps, 0, sizeof *caps);

    ret = necd_options2(sess, path, &capmask);

    caps->dav_class1     = capmask & NE_CAP_DAV_CLASS1 ? 1 : 0;
    caps->dav_class2     = capmask & NE_CAP_DAV_CLASS2 ? 1 : 0;
    caps->dav_executable = capmask & NE_CAP_MODDAV_EXEC ? 1 : 0;
    caps->dav_caldav     = capmask & NECD_CAP_CALENDAR_ACCESS ? 1 : 0;
    caps->dav_carddav    = capmask & NECD_CAP_ADDRESS_BOOK ? 1 : 0;
    return ret;
}

int debug_body_reader_accept(void *userdata, ne_request *req, const ne_status *status)
{
    /*printf("debug_body_reader_accept\n");*/
    return 1;
}

int debug_body_reader(void *userdata, const char *block, size_t len) 
{
    /*printf("debug_body_reader\n");*/
    char *sbuff = malloc(len + 10);
    memcpy(sbuff, block, len);
    *(sbuff+len+1) = 0;
    printf(sbuff);
    return NE_OK;
}

void register_debug_body_reader(ne_request *req)
{
    ne_add_response_body_reader(req, debug_body_reader_accept, debug_body_reader, NULL);
}

int necd_mkcalendar(ne_session *sess, const char *uri)
{
    ne_request *req;
    char *real_uri;
    int ret;

    if (ne_path_has_trailing_slash(uri)) {
	real_uri = ne_strdup(uri);
    } else {
	real_uri = ne_concat(uri, "/", NULL);
    }

    req = ne_request_create(sess, "MKCALENDAR", real_uri);

//#ifdef NE_HAVE_DAV
    ne_lock_using_resource(req, real_uri, 0);
    ne_lock_using_parent(req, real_uri);
//#endif
    
    ret = ne_simple_request(sess, req);

    ne_free(real_uri);

    return ret;
}

