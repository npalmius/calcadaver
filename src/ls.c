/* 
   'ls' for cadaver
   Copyright (C) 2000-2004, 2006, 2008, Joe Orton <joe@manyfish.co.uk>, 
   except where otherwise indicated.
                                                                     
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
  
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"

#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif

#ifdef HAVE_STRING_H
#include <string.h>
#endif

#include <time.h>

#include <ne_request.h>
#include <ne_props.h>
#include <ne_uri.h>
#include <ne_alloc.h>
#include <ne_dates.h>

#include "i18n.h"
#include "commands.h"
#include "cadaver.h"
#include "basename.h"
#include "utils.h"
#include "vcalendar.h"
#include "ls_cache.h"

struct fetch_context {
    struct resource **list;
    const char *target; /* Request-URI of the PROPFIND */
    unsigned int include_target; /* Include resource at href */
};    

static const ne_propname ls_props[] = {
    { "DAV:", "getcontentlength" },
    { "DAV:", "getlastmodified" },
    { "http://apache.org/dav/props/", "executable" },
    { "DAV:", "resourcetype" },
    { "DAV:", "checked-in" },
    { "DAV:", "checked-out" },
    { "DAV:", "displayname" },
    { NULL }
};

#define ELM_resourcetype (NE_PROPS_STATE_TOP + 1)
#define ELM_collection   (NE_PROPS_STATE_TOP + 2)
#define ELM_calendar     (NE_PROPS_STATE_TOP + 3)
#define ELM_addressbook  (NE_PROPS_STATE_TOP + 4)

static const struct ne_xml_idmap ls_idmap[] = {
    { "DAV:", "resourcetype", ELM_resourcetype },
    { "DAV:", "collection", ELM_collection },
    { "urn:ietf:params:xml:ns:caldav", "calendar", ELM_calendar },
    { "urn:ietf:params:xml:ns:carddav", "addressbook", ELM_addressbook}
};

static int compare_resource(const struct resource *r1, 
			    const struct resource *r2)
{
    /* Sort errors first, then collections, then alphabetically */
    if (r1->type == resr_error) {
	return -1;
    } else if (r2->type == resr_error) {
	return 1;
    } else if (r1->type == resr_collection) {
	if (r2->type != resr_collection) {
	    return -1;
	} else {
	    return strcmp(r1->uri, r2->uri);
	}
    } else {
	if (r2->type != resr_collection) {
	    return strcmp(r1->uri, r2->uri);
	} else {
	    return 1;
	}
    }
}

static void display_ls_line(struct resource *res)
{
    const char *restype;
    char exec_char, vcr_char, *name, *displayname = NULL;

    switch (res->type) {
    case resr_normal: restype = ""; break;
    case resr_reference: restype = _("Ref:"); break;
    case resr_collection: restype = _("Coll:"); break;
    case resr_calendar: restype = _("Cal:"); break;
    default:
	restype = "???"; break;
    }
    
    if (ne_path_has_trailing_slash(res->uri)) {
	res->uri[strlen(res->uri)-1] = '\0';
    }
    name = strrchr(res->uri, '/');
    if (name != NULL && strlen(name+1) > 0) {
	name++;
    } else {
	name = res->uri;
    }

    name = ne_path_unescape(name);

    if ((res->displayname != NULL) && 
        ((res->type == resr_collection) || (res->type == resr_calendar))) {
        if (strcmp(name, res->displayname)) {
            int displaylen = strlen(name) + strlen(res->displayname) + 6;
            displayname = ne_malloc(displaylen);
            snprintf(displayname, displaylen,
                     "%s   (%s)", name, res->displayname);
        }
    }
    if (displayname == NULL) {
        displayname = ne_strdup(name);
    }

    if (res->type == resr_error) {
	printf(_("Error: %-30s %d %s\n"), name, res->error_status,
	       res->error_reason?res->error_reason:_("unknown"));
    } else {
	exec_char = res->is_executable ? '*' : ' ';
	/* 0: no vcr, 1: checkin, 2: checkout */
	vcr_char = res->is_vcr==0 ? ' ' : (res->is_vcr==1? '>' : '<');
        printf("%5s %c%c%-29s %10" FMT_DAV_SIZE_T "u  %s\n",
	       restype, vcr_char, exec_char,
	       displayname, res->size, format_time(res->modtime));
    }

    free(name);
    ne_free(displayname);
}
#define MAX_CAL_DESC_LEN    40
static void display_cdls_line(struct resource *res)
{
    const char *restype;
    char exec_char, vcr_char, *name, *displayname = NULL, *tmpname = NULL;
    ne_request *req = NULL;
    vc_calendar *vcal;

    switch (res->type) {
    case resr_normal:
        vcal = lsc_get_item(res->uri, res->size, res->modtime);
        if (vcal == NULL) {
            req = ne_request_create(session.sess, "GET", res->uri);
            vcal = vc_create_calendar();
            register_calendar_body_reader(req, vcal);
            int ret = ne_request_dispatch(req);
            if (ret != 0) {
                out_result(ret);
            }
            lsc_add_item(res->uri, res->size, res->modtime, vcal);
        }

        if ((vcal->has_vcalendar) && (vc_has_parse_errors(vcal) == 0)) {
            if (vcal->has_vevent) {
                restype = _("VEVENT:");
            } else if (vcal->has_vtodo) {
                restype = _("VTODO:");
            } else if (vcal->has_vjournal) {
                restype = _("VJOURNAL:");
            } else if (vcal->has_valarm) {
                restype = _("VALARM:");
            } else if (vcal->has_vtimezone) {
                restype = _("VTIMEZONE:");
            } else if (vcal->has_vfreebusy) {
                restype = _("VFREEBUSY:");
            } else {
                restype = "";
            }

            char** caldesc = vc_get_properties(vcal, "SUMMARY");

            if (caldesc != NULL) {
                int i = 0;
                int totlen = 0, newlen = 0;
                //int count = 0
                //for (; caldesc[count] != NULL; count++);
                //printf("\n");

                for (i = 0; caldesc[i] != NULL; i++) {
                    newlen = strlen(caldesc[i]);
                    if (i == 0) {
                        tmpname = malloc(newlen + 1);
                    } else {
                        tmpname = realloc(tmpname, totlen + newlen + 3);
                        *(tmpname + totlen + 0) = ';';
                        *(tmpname + totlen + 1) = ' ';
                        totlen += 2;
                    }
                    memcpy(tmpname + totlen, caldesc[i], newlen + 1);
                    totlen += newlen;

                    if (totlen > MAX_CAL_DESC_LEN) {
                        break;
                    }
                }
                if (totlen > MAX_CAL_DESC_LEN) {
                    *(tmpname + MAX_CAL_DESC_LEN - 3) = '.';
                    *(tmpname + MAX_CAL_DESC_LEN - 2) = '.';
                    *(tmpname + MAX_CAL_DESC_LEN - 1) = '.';
                    *(tmpname + MAX_CAL_DESC_LEN - 0) = '\0';
                }

                for (i = 0; caldesc[i] != NULL; i++) {
                    free(caldesc[i]);
                }
                free(caldesc);

                //printf("\n%s\n%s", tmpname, res->displayname);
            }
        } else {
            restype = "";
        }

        //vc_destroy_calendar(vcal);
        if (req != NULL) {
            ne_request_destroy(req);
        }
        break;
    case resr_reference: restype = _("Reference:"); break;
    case resr_collection: restype = _("Collection:"); break;
    case resr_calendar: restype = _("Calendar:"); break;
    default:
        restype = "???"; break;
    }

    if (ne_path_has_trailing_slash(res->uri)) {
        res->uri[strlen(res->uri)-1] = '\0';
    }
    name = strrchr(res->uri, '/');
    if (name != NULL && strlen(name+1) > 0) {
        name++;
    } else {
        name = res->uri;
    }

    name = ne_path_unescape(name);

    if ((res->displayname != NULL) && 
        ((res->type == resr_collection) || (res->type == resr_calendar))) {
        if (strcmp(name, res->displayname)) {
            int displaylen = strlen(name) + strlen(res->displayname) + 6;
            displayname = ne_malloc(displaylen);
            snprintf(displayname, displaylen,
                     "%s   (%s)", name, res->displayname);
        }
    }
    if (tmpname != NULL) {
        int displaylen = 100;
        displayname = ne_malloc(displaylen);
        snprintf(displayname, displaylen,
                 "%-40s (%s)", name, tmpname);

        //displayname = ne_strdup(tmpname);
        free(tmpname);
    }
    if (displayname == NULL) {
        displayname = ne_strdup(name);
    }

    if (res->type == resr_error) {
        printf(_("Error: %-30s %d %s\n"), name, res->error_status,
               res->error_reason?res->error_reason:_("unknown"));
    } else {
        exec_char = res->is_executable ? '*' : ' ';
        /* 0: no vcr, 1: checkin, 2: checkout */
        vcr_char = res->is_vcr==0 ? ' ' : (res->is_vcr==1? '>' : '<');
        printf("%12s %c%c%s\n", 
               restype, vcr_char, exec_char,
               displayname);
    }

    free(name);
    ne_free(displayname);
}

void execute_ls(const char *remote)
{
    int ret;
    char *real_remote, *escaped_path;
    struct resource *reslist = NULL, *current, *next;

    if (remote != NULL) {
	escaped_path = resolve_path(session.uri.path, remote, true);
    } else {
	escaped_path = ne_strdup(session.uri.path);
    }
    real_remote = ne_path_unescape(escaped_path);
    out_start(_("Listing collection"), real_remote);
    ret = fetch_resource_list(session.sess, escaped_path, 1, 0, &reslist);
    if (ret == NE_OK) {
	/* Easy this, eh? */
	if (reslist == NULL) {
	    output(o_finish, _("collection is empty.\n"));
	} else {
	    out_success();
	    for (current = reslist; current!=NULL; current = next) {
		next = current->next;
		if (strlen(current->uri) > strlen(escaped_path)) {
		    display_ls_line(current);
		}
		free_resource(current);
	    }
	}
    } else {
	out_result(ret);
    }
    ne_free(real_remote);
    free(escaped_path);
}

void execute_cdls(const char *remote)
{
    int ret;
    char *real_remote;
    struct resource *reslist = NULL, *current, *next;

    if (remote != NULL) {
	real_remote = resolve_path(session.uri.path, remote, true);
    } else {
	real_remote = ne_strdup(session.uri.path);
    }

    //if (getrestype(real_remote) == resr_calendar) {
        out_start(_("Listing calendar"), real_remote);
        ret = fetch_resource_list(session.sess, real_remote, 1, 0, &reslist);
        if (ret == NE_OK) {
            /* Easy this, eh? */
            if (reslist == NULL) {
                output(o_finish, _("calendar is empty.\n"));
            } else {
                out_success();
                for (current = reslist; current!=NULL; current = next) {
                    next = current->next;
                    if (strlen(current->uri) > strlen(real_remote)) {
                        display_cdls_line(current);
                    }
                    free_resource(current);
                }
            }
        } else {
            out_result(ret);
        }
    /*...*/
    //} else {
    //    execute_ls(remote);
    //}

    ne_free(real_remote);
}

void execute_clearcache(void)
{
    lsc_destroy();
}

static void results(void *userdata, 
                    const ne_uri *uri,
		    const ne_prop_result_set *set)
{
    struct fetch_context *ctx = userdata;
    struct resource *current, *previous, *newres;
    const char *clength, *modtime, *isexec;
    const char *checkin, *checkout, *displayname;
    const ne_status *status = NULL;
    const char *path = uri->path;

    newres = ne_propset_private(set);

    if (ne_path_compare(ctx->target, path) == 0 && !ctx->include_target) {
	/* This is the target URI */
	NE_DEBUG(NE_DBG_HTTP, "Skipping target resource.\n");
	/* Free the private structure. */
	ne_free(newres);
	return;
    }

    newres->uri = ne_strdup(path);

    clength = ne_propset_value(set, &ls_props[0]);    
    modtime = ne_propset_value(set, &ls_props[1]);
    isexec = ne_propset_value(set, &ls_props[2]);
    checkin = ne_propset_value(set, &ls_props[4]);
    checkout = ne_propset_value(set, &ls_props[5]);
    displayname = ne_propset_value(set, &ls_props[6]);

    if (displayname != NULL) {
        newres->displayname = strdup(displayname);
    }

    if (clength == NULL)
	status = ne_propset_status(set, &ls_props[0]);
    if (modtime == NULL)
	status = ne_propset_status(set, &ls_props[1]);

    if (newres->type == resr_normal && status) {
	/* It's an error! */
	newres->error_status = status->code;

	/* Special hack for Apache 1.3/mod_dav */
	if (strcmp(status->reason_phrase, "status text goes here") == 0) {
	    const char *desc;
	    if (status->code == 401) {
		desc = _("Authorization Required");
	    } else if (status->klass == 3) {
		desc = _("Redirect");
	    } else if (status->klass == 5) {
		desc = _("Server Error");
	    } else {
		desc = _("Unknown Error");
	    }
	    newres->error_reason = ne_strdup(desc);
	} else {
	    newres->error_reason = ne_strdup(status->reason_phrase);
	}
	newres->type = resr_error;
    }

    if (isexec && strcasecmp(isexec, "T") == 0) {
	newres->is_executable = 1;
    } else {
	newres->is_executable = 0;
    }

    if (modtime)
	newres->modtime = ne_httpdate_parse(modtime);

    if (clength) {
        char *p;

        newres->size = DAV_STRTOL(clength, &p, 10);
        if (*p) {
            newres->size = 0;
        }
    }

    /* is vcr */
    if (checkin) {
	newres->is_vcr = 1;
    } else if (checkout) {
	newres->is_vcr = 2;
    } else {
	newres->is_vcr = 0;
    }

    NE_DEBUG(NE_DBG_HTTP, "End resource %s\n", newres->uri);

    for (current = *ctx->list, previous = NULL; current != NULL; 
	 previous = current, current=current->next) {
	if (compare_resource(current, newres) >= 0) {
	    break;
	}
    }
    if (previous) {
	previous->next = newres;
    } else {
	*ctx->list = newres;
    }
    newres->next = current;
}

static int ls_startelm(void *userdata, int parent, 
                       const char *nspace, const char *name, const char **atts)
{
    ne_propfind_handler *pfh = userdata;
    struct resource *r = ne_propfind_current_private(pfh);
    int state = ne_xml_mapid(ls_idmap, NE_XML_MAPLEN(ls_idmap),
                             nspace, name);

    if (r == NULL || 
        !((parent == NE_207_STATE_PROP && state == ELM_resourcetype) ||
          (parent == ELM_resourcetype && state == ELM_collection)    ||
          (parent == ELM_resourcetype && state == ELM_calendar)))
        return NE_XML_DECLINE;

    /* calendar overrides collection */
    if ((state == ELM_collection) && 
        (r->type != resr_calendar) && 
        (r->type != resr_addressbook)) {
        NE_DEBUG(NE_DBG_HTTP, "This is a collection.\n");
        r->type = resr_collection;
    } else if (state == ELM_calendar) {
        NE_DEBUG(NE_DBG_HTTP, "This is a calendar.\n");
        r->type = resr_calendar;
    } else if (state == ELM_addressbook) {
        NE_DEBUG(NE_DBG_HTTP, "This is an address book.\n");
        r->type = resr_addressbook;
    }

    return state;
}

void free_resource(struct resource *res)
{
    if (res->uri) ne_free(res->uri);
    if (res->displayname) ne_free(res->displayname);
    if (res->error_reason) ne_free(res->error_reason);
    free(res);
}

void free_resource_list(struct resource *res)
{
    struct resource *next;
    for (; res != NULL; res = next) {
	next = res->next;
	free_resource(res);
    }
}

static void *create_private(void *userdata, 
                    const ne_uri *uri
    )
{
    return ne_calloc(sizeof(struct resource));
}

int fetch_resource_list(ne_session *sess, const char *uri,
			 int depth, int include_target,
			 struct resource **reslist)
{
    ne_propfind_handler *pfh = ne_propfind_create(sess, uri, depth);

    //register_debug_body_reader(ne_propfind_get_request(pfh));

    int ret;
    struct fetch_context ctx = {0};
    
    *reslist = NULL;
    ctx.list = reslist;
    ctx.target = uri;
    ctx.include_target = include_target;

    ne_xml_push_handler(ne_propfind_get_parser(pfh), 
                        ls_startelm, NULL, NULL, pfh);

    ne_propfind_set_private(pfh, create_private, NULL, NULL);

    ret = ne_propfind_named(pfh, ls_props, results, &ctx);

    ne_propfind_destroy(pfh);

    return ret;
}
