#ifndef VCALENDAR_H
#define VCALENDAR_H

typedef struct vc_cal_p vc_cal;
typedef struct vc_item_p vc_item;

typedef struct {
    int has_vcalendar;
    int has_vtodo;
    int has_vevent;
    int has_vjournal;
    int has_vfreebusy;
    int has_vtimezone;
    int has_valarm;
    vc_cal *priv;
} vc_calendar;

vc_calendar *vc_create_calendar();
void vc_destroy_calendar(vc_calendar *vc);
void vc_print_tree(vc_calendar *vc);
int vc_has_parse_errors(vc_calendar *vc);
char** vc_get_properties(vc_calendar *vc, const char *propname);

int calendar_body_reader_accept(void *userdata, ne_request *req, const ne_status *status);
int calendar_body_reader(void *userdata, const char *block, size_t len);
void register_calendar_body_reader(ne_request *req, vc_calendar *vc);

#endif /* VCALENDAR_H */
