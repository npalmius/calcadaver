
#ifndef NECD_BASIC_H
#define NECD_BASIC_H

/* DEPRECATED: Server capabilities. */
typedef struct {
    unsigned int dav_class1; /* True if Class 1 WebDAV server */
    unsigned int dav_class2; /* True if Class 2 WebDAV server */
    unsigned int dav_executable; /* True if supports the 'executable'
				  * property a. la. mod_dav */
    unsigned int dav_caldav; /* True if CalDAV Server */
    unsigned int dav_carddav; /* True if CalDAV Server */
} necd_server_capabilities;

/* DEPRECATED: Determines server capabilities (using OPTIONS). */
int necd_options(ne_session *sess, const char *path,
               necd_server_capabilities *caps);


#define NECD_CAP_CALENDAR_ACCESS        (0x00080000) /* CalDAV Collection */
#define NECD_CAP_CALENDAR_SCHEDULE      (0x00100000) /* CalDAV Collection */
#define NECD_CAP_CALENDAR_SCHEDULE_AUTO (0x00200000) /* CalDAV Collection */
#define NECD_CAP_CALENDAR_PROXY         (0x00400000) /* CalDAV Collection */
#define NECD_CAP_EXTENDED_MKCOL         (0x00800000) /* CalDAV Collection */
#define NECD_CAP_BIND                   (0x01000000) /* CalDAV Collection */
#define NECD_CAP_ADDRESS_BOOK           (0x02000000) /* CalDAV Collection */

/* Determines resource capailities, using an OPTIONS request.  On
 * return, *caps is set to a bit-mask of the NE_CAP_* constants
 * describing the advertised resource capabilities. */
int necd_options2(ne_session *sess, const char *path, unsigned int *caps);

int debug_body_reader_accept(void *userdata, ne_request *req, const ne_status *status);
int debug_body_reader(void *userdata, const char *block, size_t len);
void register_debug_body_reader(ne_request *req);

int necd_mkcalendar(ne_session *sess, const char *uri);

//ne_propfind_handler *necd_propfind_create(ne_session *sess, const char *uri, int depth);
#define necd_propfind_create ne_propfind_create
#endif /* NECD_BASIC_H */
