#include "config.h"

#ifdef HAVE_STDLIB_H
#include <stdlib.h>
#endif

#ifdef HAVE_STRING_H
#include <string.h>
#endif
#include <stdint.h>

#include <ne_request.h>

#include "lookup3.h"
#include "vcalendar.h"
#include "ls_cache.h"

typedef struct _lsc_item {
    char *key;
    size_t size;
    time_t modtime;
    void *item;
} lsc_item;


static lsc_item **cache = NULL;
static int cache_size = 0;

static void create_hashes(unsigned char *str, uint32_t *hash1, uint32_t *hash2)
{
    *hash1 = 654323;
    *hash2 = 123457;
    hashlittle2(str, strlen(str), hash1, hash2);
    *hash1 %= cache_size;
    *hash2 %= cache_size;
}
static unsigned int create_hash1(unsigned char *str)
{
    uint32_t hash1;
    uint32_t hash2;
    create_hashes(str, &hash1, &hash2);
    return hash1 % cache_size;
}
static unsigned int create_hash2(unsigned char *str)
{
    uint32_t hash1;
    uint32_t hash2;
    create_hashes(str, &hash1, &hash2);
    return hash2 % cache_size;
}

void lsc_init(int size)
{
    if (cache == NULL) {
        cache = malloc(size * sizeof(lsc_item *));
        memset(cache, 0, size * sizeof(lsc_item *));
        cache_size = size;
    }
}

void lsc_destroy(void)
{
    if (cache != NULL) {
        int i;
        for (i = 0; i < cache_size; i++) {
            lsc_remove_item_by_hash(i);
        }
        free(cache);
        cache = NULL;
    }
}

/* cache[hash] MUST exist */
static void destroy_item(int hash)
{
    free(cache[hash]->key);
    vc_destroy_calendar((vc_calendar *)cache[hash]->item);
    free(cache[hash]);
    cache[hash] = NULL;
}

void lsc_remove_item_by_hash(int hash)
{
    if (cache == NULL) {
        return;
    } else if (cache[hash] == NULL) {
        return;
    }

    if (cache[hash]->key != NULL) {
        destroy_item(hash);
    }
}

void lsc_remove_item_by_key(char *key)
{
    uint32_t hash1, hash2;

    if ((cache == NULL) || (key == NULL)) {
        return;
    }

    create_hashes(key, &hash1, &hash2);

    /* check both hashes */
    if ((cache[hash1] != NULL) && (strcmp(cache[hash1]->key, key) == 0)) {
        destroy_item(hash1);
    }
    if ((cache[hash2] != NULL) && (strcmp(cache[hash2]->key, key) == 0)) {
        destroy_item(hash2);
    }
}

int lsc_add_item(char *key, size_t size, time_t modtime, vc_calendar *vc)
{
    uint32_t hash1, hash2;

    if (cache == NULL) {
        lsc_init(LSC_CACHE_SIZE);
        if (cache == NULL) {
            return -1;
        }
    }

    if (key == NULL) {
        return -1;
    }

    create_hashes(key, &hash1, &hash2);

    if ((cache[hash1] != NULL) && (strcmp(cache[hash1]->key, key) == 0)) {
        lsc_remove_item_by_hash(hash2);
    }
    if ((cache[hash2] != NULL) && (strcmp(cache[hash2]->key, key) == 0)) {
        lsc_remove_item_by_hash(hash2);
    }

    lsc_item *newitem = malloc(sizeof(lsc_item));

    newitem->key = malloc(strlen(key) + 1);
    memcpy(newitem->key, key, strlen(key) + 1);
    newitem->item = vc;
    newitem->size = size;
    newitem->modtime =modtime;

    if (cache[hash1] == NULL) {
        cache[hash1] = newitem;
        return hash1;
    } else if (cache[hash2] == NULL) {
        cache[hash2] = newitem;
        return hash2;
    } else {
        cache[hash1] = newitem;
        return hash2;
    }
}

vc_calendar *lsc_get_item(char *key, size_t size, time_t modtime)
{
    uint32_t hash1, hash2;

    if ((cache == NULL) || (key == NULL)) {
        return NULL;
    }

    create_hashes(key, &hash1, &hash2);

    if ((cache[hash1] != NULL) && (strcmp(cache[hash1]->key, key) == 0) &&
        (cache[hash1]->size == size) && (cache[hash1]->modtime == modtime)) {
        return cache[hash1]->item;
    } else if ((cache[hash2] != NULL) && (strcmp(cache[hash2]->key, key) == 0) &&
        (cache[hash2]->size == size) && (cache[hash2]->modtime == modtime)) {
        return cache[hash2]->item;
    //} else if (strcmp(cache[hash]->key, key)) {
    //    return NULL;
    //} else if ((cache[hash]->size == size) && (cache[hash]->modtime == modtime)){
    //    return cache[hash]->item;
    } else {
        return NULL;
    }
}
