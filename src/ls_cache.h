#ifndef LSCACHE_H
#define LSCACHE_H

#define LSC_CACHE_SIZE 1009

void lsc_init(int size);
void lsc_destroy(void);
void lsc_remove_item_by_hash(int hash);
void lsc_remove_item_by_key(char *key);
int lsc_add_item(char *key, size_t size, time_t modtime, vc_calendar *vc);
vc_calendar *lsc_get_item(char *key, size_t size, time_t modtime);


#endif /* LSCACHE_H */
