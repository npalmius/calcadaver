#include <stdio.h>
#include <string.h>

#include <ne_basic.h>
#include <ne_request.h>
#include "vcalendar.h"

struct vc_item_p {
    vc_item *next;
    vc_item *parent;
    vc_item *child;

    enum {
        VC_COMP_OTHER,
        VC_COMP_VCALENDAR,
        VC_COMP_VEVENT,
        VC_COMP_VTODO,
        VC_COMP_VJOURNAL,
        VC_COMP_VFREEBUSY,
        VC_COMP_VTIMEZONE,
        VC_COMP_VTZ_STANDARD,
        VC_COMP_VTZ_DAYLIGHT,
        VC_COMP_VALARM
    } component_type;
    char *name;
    enum {
        VC_COMPONENT,
        /*VC_PARAMETER,*/
        VC_PROPERTY
    } type;

    union {
        struct {
            char *parameters_begin;
            char *parameters_end;
        } component;
        struct {
            char *parameters;
            char *property;
        } property;
    };
};

struct vc_cal_p {
    int datasize;
    char *data;
    vc_item *first;
    vc_item *current_component;
    vc_item *current_item;
    int parse_errors;
};


vc_calendar *vc_create_calendar()
{
    vc_calendar *vc = malloc(sizeof(vc_calendar));
    memset(vc, 0, sizeof(vc_calendar));
    vc->priv = malloc(sizeof(vc_cal));
    memset(vc->priv, 0, sizeof(vc_cal));

    return vc;
}

static void get_properties_branch(vc_item *branch, const char *propname,
                                  char ***props, int *propcount)
{
    if (branch == NULL) {
        return;
    }

    if (branch->type == VC_COMPONENT) {
        vc_item *item = branch->child;
        while (item != NULL) {
            get_properties_branch(item, propname, props, propcount);
            item = item->next;
        }
    } else if (strcmp(branch->name, propname) == 0) {
        (*propcount)++;
        *props = realloc(*props, (*propcount) * sizeof(char*));
        int proplen = strlen(branch->property.property);
        int offset = (*propcount) - 1;
        (*props)[offset] = malloc(proplen+1);
        memcpy((*props)[offset], branch->property.property, proplen + 1);
    }
}

char** vc_get_properties(vc_calendar *vc, const char *propname)
{
    char **props = NULL;// malloc(100);//NULL;
    int propcount = 0;

    if ((vc == NULL) || (vc->priv == NULL) || (vc->priv->first == NULL)) {
        return NULL;
    } else {
        get_properties_branch(vc->priv->first, propname, &props, &propcount);
        if (props != NULL) {
            props = realloc(props, (propcount + 1) * sizeof(char *));
            props[propcount] = NULL;
        }
        return props;
    }
}

static void destroy_branch(vc_item *branch)
{
    if (branch == NULL) {
        return;
    }

    if (branch->type == VC_COMPONENT) {
        vc_item *item = branch->child;
        vc_item *next = NULL;
        while (item != NULL) {
            next = item->next;
            destroy_branch(item);
            item = next;
        }
    }
    if (branch->name != NULL) {
        free(branch->name);
    }
    if (branch->type == VC_COMPONENT) {
        if (branch->component.parameters_begin != NULL) {
            free(branch->component.parameters_begin);
        }
        if (branch->component.parameters_end != NULL) {
            free(branch->component.parameters_end);
        }
    } else if (branch->type == VC_PROPERTY) {
        if (branch->property.parameters != NULL) {
            free(branch->property.parameters);
        }
        if (branch->property.property != NULL) {
            free(branch->property.property);
        }
    }
    free(branch);
}

int vc_has_parse_errors(vc_calendar *vc)
{
    if ((vc != NULL) && (vc->priv != NULL)) {
        return vc->priv->parse_errors;
    } else {
        return 1;
    }
}

void vc_destroy_calendar(vc_calendar *vc)
{
    destroy_branch(vc->priv->first);
    if (vc->priv->data != NULL) {
        free(vc->priv->data);
    }
    free(vc->priv);
    free(vc);
} 

static char *get_min_pos(char *c1, char *c2)
{
    if ((c1 == NULL) && (c2 == NULL)) {
        return NULL;
    } else if (c1 == NULL) {
        return c2;
    } else if (c2 == NULL) {
        return c1;
    } else if (c1 < c2) {
        return c1;
    } else if (c1 > c2) {
        return c2;
    } else {
        return c1;
    }
}

static char *get_next_newline(const char *data)
{
    char *cn = strchr(data, '\n');
    char *cr = strchr(data, '\r');

    return get_min_pos(cn, cr);
}

static char *get_next_line(const char *data, int *len)
{
    char *nextchr = get_next_newline(data);
    int nextnl;

    if (nextchr == NULL) {
        nextnl = strlen(data);
    } else {
        nextnl = nextchr - data;
    }

    char *ret = NULL;

    if (nextnl > 0) {
        ret = malloc(nextnl + 1);
        strncpy(ret, data, nextnl);
        *(ret+nextnl) = 0;
    }

    if (len != NULL) {
        *len = nextnl;
    }

    return ret;
}

static void process_line(char *line, vc_calendar *vc)
{
    if (line == NULL) {
        return;
    } else if (strlen(line) == 0) {
        return;
    }

    char *ccolon = strchr(line, ':');
    char *tok_end = get_min_pos(ccolon, strchr(line, ';'));

    if (tok_end == NULL) {
        return;
    }

    /* TODO: check for quotes/escapes in parameters */
    char *propname = malloc(tok_end - line + 1);
    memset(propname, 0, tok_end - line + 1);
    memcpy(propname, line, tok_end - line);

    char *params;
    if (ccolon - tok_end <= 1) {
        params = malloc(1);
        *params = 0;
    } else {
        params = malloc(ccolon - tok_end);
        memset(params, 0, ccolon - tok_end);
        memcpy(params, tok_end + 1, ccolon - tok_end - 1);
    }

    int proplen = strlen(line) - (ccolon - line);
    char *prop = malloc(proplen + 1);
    memset(prop, 0, proplen + 1);
    memcpy(prop, ccolon + 1, proplen);

    //printf("%s\n%s\n%s\n%s\n\n", line, propname, params, prop);
    
    if (strcmp(propname, "BEGIN") == 0) {
        vc_item *item = malloc(sizeof(vc_item));
        memset(item, 0, sizeof(vc_item));

        item->type = VC_COMPONENT;
        item->name = prop;
        item->parent = vc->priv->current_component;
        item->component.parameters_begin = params;

        if (vc->priv->first == NULL) {
            vc->priv->first = item;
        } else if (vc->priv->current_component == NULL) {
            vc->priv->parse_errors = 1;
            free(propname);
            free(params);
            free(prop);
            free(item);
            return;
        } else if (vc->priv->current_component->child == NULL) {
            vc->priv->current_component->child = item;
        } else if (vc->priv->current_item == NULL) {
            vc->priv->parse_errors = 1;
            free(propname);
            free(params);
            free(prop);
            free(item);
            return;
        } else {
            vc->priv->current_item->next = item;
        }

        if (!strcmp(prop, "VEVENT")) {
            item->component_type = VC_COMP_VEVENT;
            vc->has_vevent++;
        } else if (!strcmp(prop, "VTODO")) {
            item->component_type = VC_COMP_VTODO;
            vc->has_vtodo++;
        } else if (!strcmp(prop, "VCALENDAR")) {
            item->component_type = VC_COMP_VCALENDAR;
            vc->has_vcalendar++;
        } else if (!strcmp(prop, "VJOURNAL")) {
            item->component_type = VC_COMP_VJOURNAL;
            vc->has_vjournal++;
        } else if (!strcmp(prop, "VFREEBUSY")) {
            item->component_type = VC_COMP_VFREEBUSY;
            vc->has_vfreebusy++;
        } else if (!strcmp(prop, "VTIMEZONE")) {
            item->component_type = VC_COMP_VTIMEZONE;
            vc->has_vtimezone++;
        } else if (!strcmp(prop, "VALARM")) {
            item->component_type = VC_COMP_VALARM;
            vc->has_valarm++;
        } else if ((!strcmp(prop, "STANDARD")) && (item->parent != NULL) && 
                   (item->parent->component_type = VC_COMP_VTIMEZONE)) {
            item->component_type = VC_COMP_VTZ_STANDARD;
        } else if ((!strcmp(prop, "DAYLIGHT")) && (item->parent != NULL) && 
                   (item->parent->component_type = VC_COMP_VTIMEZONE)) {
            item->component_type = VC_COMP_VTZ_DAYLIGHT;
        } else {
            item->component_type = VC_COMP_OTHER;
        }

        vc->priv->current_component = item;
        vc->priv->current_item = item;

        free(propname);

    } else if (strcmp(propname, "END") == 0) {
        if (vc->priv->current_component == NULL) {
            vc->priv->parse_errors = 1;
            free(propname);
            free(params);
            free(prop);
            return;
        } else if (strcmp(vc->priv->current_component->name, prop)) {
            vc->priv->parse_errors = 1;
            free(propname);
            free(params);
            free(prop);
            return;
        }

        vc->priv->current_component->component.parameters_end = params;
        vc->priv->current_item = vc->priv->current_component;
        vc->priv->current_component = vc->priv->current_component->parent;

        free(propname);
        free(prop);
    } else {
        vc_item *item = malloc(sizeof(vc_item));
        memset(item, 0, sizeof(vc_item));

        item->type = VC_PROPERTY;
        item->name = propname;
        item->parent = vc->priv->current_component;
        item->property.parameters = params;
        item->property.property = prop;

        if ((vc->priv->first == NULL) || (vc->priv->current_component == NULL)) {
            vc->priv->parse_errors = 1;
            free(propname);
            free(params);
            free(prop);
            return;
        } else if (vc->priv->current_component->child == NULL) {
            vc->priv->current_component->child = item;
        } else if (vc->priv->current_item == NULL) {
            vc->priv->parse_errors = 1;
            free(propname);
            free(params);
            free(prop);
            return;
        } else {
            vc->priv->current_item->next = item;
        }
        if (item->parent != NULL) {
            item->component_type = item->parent->component_type;
        }
        vc->priv->current_item = item;
    }
}

static void print_branch(vc_item *branch, int depth)
{
    if (branch == NULL) {
        return;
    }
    int i;
    for (i = 0; i < depth; i++) {
        printf(" ");
    }

    if (branch->type == VC_COMPONENT) {
        printf("%i %-15s\n", branch->component_type, branch->name);
    } else {
        printf("%i %-15s\t- %s\n", branch->component_type, branch->name,
                                  branch->property.property);
    }
    if (branch->type == VC_COMPONENT) {
        vc_item *item = branch->child;
        while (item != NULL) {
            print_branch(item, depth + 1);
        item = item->next;
        }
    }
}

void vc_print_tree(vc_calendar *vc)
{
    if ((vc == NULL) || (vc->priv == NULL) || (vc->priv->first == NULL)) {
        printf("No tree to print!!");
    } else {
        printf("\n\nParse Errors : %s\n\n", vc->priv->parse_errors?"Yes":"No");
        print_branch(vc->priv->first, 0);
        printf("\n");
    }
}

static void process_calendar_data(vc_calendar *vc)
{
    char *line = NULL;
    char *nextline = NULL;
    int cont = 0;
    int pos = 0;
    int inc = 0;

    line = get_next_line(vc->priv->data+pos, &inc);
    if (line == NULL) {
        return;
    }
    do {
        pos += inc;

        while ((*(vc->priv->data+pos) == '\n') || (*(vc->priv->data+pos) == '\r'))
            pos++;

        nextline = get_next_line(vc->priv->data+pos, &inc);
        if ((nextline != NULL) && ((*nextline == ' ') || (*nextline == '\t'))) {
            line = realloc(line, strlen(line) + strlen(nextline) + 1);
            memcpy(line + strlen(line), nextline+1, strlen(nextline));
            free(nextline);
        } else {
            process_line(line, vc);
            free(line);
            line = nextline;
        }
    } while (line != NULL);
    //print_tree(vc);
}

int calendar_body_reader_accept(void *userdata, ne_request *req, const ne_status *status)
{
    ne_content_type nect;

    ne_get_content_type(req, &nect);

    if (strcmp(nect.type, "text") || (strcmp(nect.subtype, "calendar"))) {
        return 0;
    } else {
        return 1;
    }
}

int calendar_body_reader(void *userdata, const char *block, size_t len)
{
    vc_calendar *vc = userdata;

    if (len > 0) {
        vc->priv->data = realloc(vc->priv->data, vc->priv->datasize + len + 1);
        memset(vc->priv->data + vc->priv->datasize, 0, len + 1);
        memcpy(vc->priv->data + vc->priv->datasize, block, len);
        vc->priv->datasize += len;
    } else {
        //FILE *f = fopen("test.txt", "w");
        //fputs(vc->data, f);
        //fclose(f);
        process_calendar_data(vc);
    }

    return NE_OK;
}

void register_calendar_body_reader(ne_request *req, vc_calendar *vc)
{
    ne_add_response_body_reader(req, calendar_body_reader_accept,
                                calendar_body_reader, vc);
}

